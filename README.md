## RootNode Coding Assignment

Each Node represents an element of a tree and specifies a list of immediate children. The 'Children'
property lists all children (in order) but the 'Right' property is set to null.

Suppose you are given the root of a fully populated tree (i.e. a Node instance called rootNode).
Write code to set the 'Right' property so that each node is linked to right siblings without using a
queue or stack. Make sure to test your code with the sample tree below.

To get you started:
	
	public class Node {
		public Node[] Children;
		public Node Right;
	}
Node rootNode;

