package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import sample.util;

public enum NodePropertySetter {
	INSTANCE;
	private String TAG = this.getClass().getName();	
	
	/**
	 * Interate Node and set the right property to the node
	 * @param input Node
	 * @return
	 */
	public boolean SetRightNode(Node parentNode){
		
		if(parentNode == null || parentNode.getChildren() == null || parentNode.getChildren().isEmpty()){
			return false;
		}
		
		SetRightNodeImpl(parentNode);
		
		return false;
		
	}

	
	private void SetRightNodeImpl(Node rootNode){		
		
		List <Node> nodeList = rootNode.getChildren();
		int nodeSize = rootNode.getChildren().size();
		
		for(int i=0; i<nodeSize; i++){
			Node currentNode = nodeList.get(i);
			gprintf(TAG, "Iterate " + currentNode.getData());
			
			//Check Node next to current node in the same branch
			if(i+1 < nodeSize){
				gprintf(TAG, "Found right node for "+ currentNode.getData() + " = " + nodeList.get(i+1).getData());
				currentNode.setRight(nodeList.get(i+1));
				
			//Check Node next to current node in the other branch
			} else {
				Node nextSameLevelNode = getSameLevelRightNode(currentNode, currentNode.getLevel());
				
				gprintf(TAG, "Searching right node for "+ currentNode.getData());
				
				if(nextSameLevelNode != null){
					currentNode.setRight(nextSameLevelNode);
					gprintf(TAG, "Found right node for "+ currentNode.getData() + " = " + nextSameLevelNode.getData());
				}
				
			}
			
			SetRightNodeImpl(currentNode);
			
		}
	}
	
	private Node getSameLevelRightNode(Node currentNode, int level){
				
		Node parentNode = currentNode.getParent();
		if( currentNode.getParent() == null){
			return null;
		}
		
		Node superParentNode =parentNode.getParent();
		if(superParentNode == null){
			return null;
		}
		gprintf(TAG, "Super parent: " + superParentNode.getData());
		
		int parentIdx = superParentNode.getChildren().indexOf(parentNode);
		int superParentChildSize = superParentNode.getChildren().size();
		int nextParentIdx = parentIdx + 1;
		if(nextParentIdx > superParentChildSize){
			return null;
		}
		gprintf(TAG, "nextParentIdx: " + nextParentIdx);
		
		ListIterator<Node> litr = superParentNode.getChildren().listIterator(nextParentIdx);
		
		while(litr.hasNext()){
			
			Node nextNode = (Node) litr.next();
			if(nextNode.getChildren() != null && nextNode.getChildren().size() != 0){
				//Have child
				return nextNode.getChildren().get(0);
			}
		}		
		
		return null;
	}
	
	private void gprintf(String TAG, String msg){		
		util.INSTANCE.gprintf(TAG, msg);
	}

}
