package sample;

import java.util.ArrayList;
import java.util.List;

public class Node {
	
	private List<Node> Children = new ArrayList<Node>();
	private Node Parent = null;
	private Node Right = null;
	private String Data = null;
	private int level = 0;
	
	//Constructor
	public Node(String data){
		this.Data = data;
	}
	
	public Node(String data, Node Parent){
		this.Data = data;
		this.Parent = Parent;
	}	

	//Getter & Setter
	public List<Node> getChildren() {
		return Children;
	}
	
	public String getData() {
		return Data;
	}

	public void setData(String data) {
		Data = data;
	}
	
	public void setParent(Node Parent){
		this.Parent = Parent;
	}	
	
	public Node getParent() {
		return Parent;
	}

	public void addChild(String Data){
		Node child = new Node (Data);
		child.setParent(this);
		child.setLevel(calculateLevel(child));
		this.Children.add(child);
	}
	
	public void addChild(Node child){
		child.setParent(this);
		child.setLevel(calculateLevel(child));
		this.Children.add(child);
	}

	public Node getRight() {
		return Right;
	}

	public void setRight(Node right) {
		this.Right = right;
	}	
	
	
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	private int calculateLevel(Node child){
		int level = 0;
		
		Node idxNode = child;
		while (idxNode.getParent() != null){
			level++;
			idxNode = idxNode.getParent();
		}
		
		return level;
		
	}
	
}
