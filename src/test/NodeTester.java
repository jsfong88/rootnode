package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sample.Node;
import sample.NodePropertySetter;
import sample.util;

public class NodeTester {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void Test01() {
		System.out.println("Test01");
		
		Node parentNode = new Node ("Parent");
		Node childNode1 = new Node ("child01", parentNode);		
		Node childNode2 = new Node ("child02", parentNode);
		parentNode.addChild(childNode1);
		parentNode.addChild(childNode2);
		

		List<Node> childList = parentNode.getChildren();	
		
		for(Node node: childList){			
			System.out.println(node.getData() + " level : " + node.getLevel());
		}
		assertTrue(!childList.isEmpty());
	}
	
	@Test
	public void Test_setRight01_1Level(){
		gprintf("Test", "Test_setRight01_1Level");
		
		Node parentNode = new Node ("Parent");
		Node childNode1 = new Node ("child01", parentNode);		
		Node childNode2 = new Node ("child02", parentNode);		
		Node childNode3 = new Node ("child03", parentNode);
		parentNode.addChild(childNode1);
		parentNode.addChild(childNode2);
		parentNode.addChild(childNode3);
		
		NodePropertySetter.INSTANCE.SetRightNode(parentNode);	
		
		
		assertNotNull(childNode1.getRight());
		assertEquals(childNode1.getRight(), childNode2);
		
		assertNotNull(childNode2.getRight());		
		assertEquals(childNode2.getRight(), childNode3);
		
		assertNull(childNode3.getRight());
				
			
		
	}
	
	@Test
	public void Test_setRight02_2Level(){
		gprintf("Test", "Test_setRight02_2Level");
		
		//setup node
		Node parentNode = new Node ("Parent");
		Node childNode1 = new Node ("child01", parentNode);		
		Node childNode2 = new Node ("child02", parentNode);		
		Node childNode3 = new Node ("child03", parentNode);
		Node childNode4 = new Node ("child04", childNode1);
		Node childNode5 = new Node ("child05", childNode1);
		Node childNode6 = new Node ("child06", childNode3);
		parentNode.addChild(childNode1);
		parentNode.addChild(childNode2);
		parentNode.addChild(childNode3);
		childNode1.addChild(childNode4);
		childNode1.addChild(childNode5);
		childNode3.addChild(childNode6);
		
		NodePropertySetter.INSTANCE.SetRightNode(parentNode);
		
		System.out.println(childNode4.getData() + " level : " + childNode4.getLevel());
		
		assertNotNull(childNode1.getRight());
		assertEquals(childNode1.getRight(), childNode2);
		
		assertNotNull(childNode2.getRight());		
		assertEquals(childNode2.getRight(), childNode3);
		
		assertNotNull(childNode4.getRight());
		assertEquals(childNode4.getRight(), childNode5);
		
		assertNotNull(childNode5.getRight());
		assertEquals(childNode5.getRight(), childNode6);
		
		assertNull(childNode6.getRight());
		
	}
	
	@Test
	public void Test_setRight03_3Level(){
		gprintf("Test", "Test_setRight03_3Level");
		
		//setup node
		Node parentNode = new Node ("Parent");
		Node childNode1 = new Node ("child01", parentNode);		
		Node childNode2 = new Node ("child02", parentNode);		
		Node childNode3 = new Node ("child03", parentNode);
		Node childNode4 = new Node ("child04", childNode1);
		Node childNode5 = new Node ("child05", childNode1);
		Node childNode6 = new Node ("child06", childNode3);
		Node childNode7 = new Node ("child07", childNode4);
		Node childNode8 = new Node ("child08", childNode4);
		Node childNode9 = new Node ("child09", childNode4);
		parentNode.addChild(childNode1);
		parentNode.addChild(childNode2);
		parentNode.addChild(childNode3);
		childNode1.addChild(childNode4);
		childNode1.addChild(childNode5);
		childNode3.addChild(childNode6);
		childNode4.addChild(childNode7);
		childNode4.addChild(childNode8);
		childNode4.addChild(childNode9);
		
		NodePropertySetter.INSTANCE.SetRightNode(parentNode);
		
		assertNotNull(childNode1.getRight());
		assertEquals(childNode1.getRight(), childNode2);
		
		assertNotNull(childNode2.getRight());		
		assertEquals(childNode2.getRight(), childNode3);
		
		assertNotNull(childNode4.getRight());
		assertEquals(childNode4.getRight(), childNode5);
		
		assertNotNull(childNode5.getRight());
		assertEquals(childNode5.getRight(), childNode6);
		
		assertNull(childNode6.getRight());
		
		assertNotNull(childNode7.getRight());
		assertEquals(childNode7.getRight(), childNode8);
		
		assertNotNull(childNode8.getRight());
		assertEquals(childNode8.getRight(), childNode9);
		
		assertNull(childNode9.getRight());
		
	}
	
	private void gprintf(String TAG, String msg){		
		util.INSTANCE.gprintf(TAG, msg);
	}

}
